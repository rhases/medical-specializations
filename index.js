var medicalInfos = {};

if (typeof require !== 'undefined') {
	var _ = require('lodash');
}

medicalInfos.specializations = [
   {
      cod: 'acupunturista',
      label: 'Acupunturista',
      decription: 'ramo da medicina tradicional chinesa e um método de tratamento chamado complementar de acordo com a nova terminologia da OMS. Obs: O Conselho Federal de Medicina baixa resolução atribuindo aos médicos a exclusividade do uso da acupuntura, porém é importante salientar que o Conselho Federal de Medicina tentou, primeiro por meio da imprensa,  desqualificar os profissionais de curso superior da área da saúde ( não médicos ) que praticam a acupuntura, sem conseguir tentaram por meio dos Tribunais de Justiça caçar as Resoluções dos Conselhos Federais que regulamentaram a Acupuntura porém... perderam  no Superior Tribunal de Justiça e no Supremo Tribunal Federal. Os despachos dos Tribunais esclarecem: 1 – Acupuntura não é prática regulamentada por Lei Federal, portanto, em conformidade com o artigo 5º da Constituição Federal é livre o seu exercício até que o Congresso Nacional a regulamente por lei própria. 2 – Acupuntura não pode ser classificada como ato médico uma vez que na China não é exercida por médicos alopatas e difere substancialmente dos métodos gnosiológicos da medicina ocidental, sendo seu diagnóstico e terapêutica próprios; E para concluir o CFM reconhece como habilitação o curso de acupuntura na medicina e não especialidade, sendo os únicos conselhos a reconhecer como especialistas são entretanto, apenas 3 Conselhos que colocam Acupuntura como especialização (COFFITO, COFEN e CFBM), os outros baixaram resoluções apenas aceitando como habilitação (então tais profissionais não podem ser chamados de especialistas); com as sentenças do TRF 1ª, COFFITO, COFEN, CFP, CFF e CFFono ((o CFBM e o CONFEF não foram afetados)'
   },
   {
      cod: 'imunologista',
      label: 'Imunologista',
      otherLabels: [
         'Alergista'
      ],
      decription: 'diagnóstico e tratamento das doenças alérgicas e do sistema imunitário.'
   },
   {
      cod: 'anestesiologista',
      label: 'Anestesiologista',
      decription: 'estudo da dor e anestesia.'
   },
   {
      cod: 'angiologista',
      label: 'Angiologista',
      decription: 'é a área da medicina que estuda o tratamento das doenças do aparelho circulatório.'
   },
   {
      cod: 'oncologista',
      label: 'Oncologista',
      otherLabels: [
         'Cancerologia'
      ],
      decription: 'é a especialidade que estuda os tumores malignos ou câncer.'
   },
   {
      cod: 'cardiologista',
      label: 'Cardiologista',
      decription: 'estudo das doenças relacionadas com o coração.'
   },
   {
      cod: 'cirurgiao-cardiovascular',
      label: 'Cirurgião Cardiovascular',
      decription: 'tratamento cirúrgico de doenças do coração.'
   },
   {
      cod: 'cirurgiao-da-mao',
      label: 'Cirurgião da Mão',
      decription: ''
   },
   {
      cod: 'cirurgiao-de-cabeca-e-pescoco',
      label: 'Cirurgião de Cabeça e Pescoço',
      decription: 'tratamento cirúrgico de doenças da cabeça e do pescoço.'
   },
   {
      cod: 'cirurgiao-do-aparelho-digestivo',
      label: 'Cirurgião do Aparelho Digestivo',
      decription: 'atua na cirurgia dos órgãos do aparelho digestório, como o esôfago, estômago, intestinos, fígado e vias biliares, e pâncreas.'
   },
   {
      cod: 'cirurgiao-geral',
      label: 'Cirurgião Geral',
      decription: 'é a área que engloba todas as áreas cirúrgicas, sendo também subdividida.'
   },
   {
      cod: 'cirurgiao-pediatra',
      label: 'Cirurgião Pediátra',
      decription: 'cirurgia geral em crianças.'
   },
   {
      cod: 'cirurgiao-plastico',
      label: 'Cirurgião Plástico',
      decription: 'tratamento para correção das deformidades, má formação ou lesões que comprometem funções dos órgãos através de cirurgia de caráter reparador.'
   },
   {
      cod: 'cirurgiao-toracico',
      label: 'Cirurgião Torácico',
      decription: 'atua na cirurgia dos pulmões.'
   },
   {
      cod: 'cirurgiao-vascular',
      label: 'Cirurgião Vascular',
      decription: 'tratamento das veias e artérias, através de cirurgia.'
   },
   {
      cod: 'clinico-medico',
      label: 'Clínico Médico',
      decription: 'é a área que engloba todas as áreas não cirurgicas, sendo subdividida em várias outras especialidades.'
   },
   {
      cod: 'proctologista',
      label: 'Proctologista',
      otherLabels: [
         'Coloproctologista'
      ],
      decription: 'é a parte da medicina que estuda e trata os problemas do intestino grosso (cólon), sigmoide e doenças do reto, canal anal e ânus.'
   },
   {
      cod: 'dermatologista',
      label: 'Dermatologista',
      decription: 'é o estudo da pele e suas doenças.'
   },
   {
      cod: 'endocrinologista',
      label: 'Endocrinologista',
      decription: 'é o tratamento das glândulas.'
   },
   {
      cod: 'gastroenterologista',
      label: 'Gastroenterologista',
      decription: 'é o tratamento do aparelho digestivo.'
   },
   {
      cod: 'geriatra',
      label: 'Geriatra',
      decription: ' é o estudo das doenças do idoso.'
   },
   {
      cod: 'ginecologista',
      label: 'Ginecologista',
      decription: 'e obstetrícia: é o estudo do sistema reprodutor feminino.'
   },
   {
      cod: 'hematologista',
      label: 'Hematologista',
      decription: 'é o estudo dos elementos figurados do sangue (hemácias, leucócitos, plaquetas) e da produção desses elementos nos órgãos hematopoiéticos (medula óssea, baço, linfonódos).'
   },
   {
      cod: 'homeopata',
      label: 'Homeopata',
      decription: 'é a prática médica baseada na Lei dos Semelhantes. (Considerado pseudociência pela comunidade científica por não apresentar provas científicas da sua eficácia.) [2] [3] [4] [5] [6] [7] [8]'
   },
   {
      cod: 'infectologista',
      label: 'Infectologista',
      decription: 'estudo das causas e tratamentos de infecções (causadas por vírus, bacterias, fungos e parasitas (helmintologia, protozoologia, entomologia e artropodologia)'
   },
   {
      cod: 'mastologista',
      label: 'Mastologista',
      decription: 'tratamento de doenças da mama.'
   },
   {
      cod: 'medico-da-familia',
      label: 'Medico da Família',
      decription: 'é a área da medicina que se propõe a estudar o indivíduo enquanto ser inserido num contexto familiar e comunitário, nomeadamente integrado na sua família. Procura entender como este indivíduo se relaciona com os grupos sociais e estuda as doenças que o acometem através deste prisma. Trabalha preferencialmente com actividades de prevenção, mas também de uma Medicina de abordagem geral. Idealmente consegue resolver a vasta maioria das doenças de alta prevalência, seu principal objecto.'
   },
   {
      cod: 'medico-do-trabalho',
      label: 'Medico do Trabalho',
      decription: 'trata das doenças causadas pelo trabalho ou com este relacionadas.'
   },
   {
      cod: 'medico-esportista',
      label: 'Medico Esportista',
      decription: 'vocacionada para a abordagem do atleta de uma forma global, suas áreas estendem-se desde a fisiologia do exercício à prevenção de lesões, passando pelo controle de treino e resolução de todo e qualquer problema de saúde que envolva o praticante de desporto ou simplesmente, exercício físico.'
   },
   {
      cod: 'medico-intensivista',
      label: 'Medico Intensivista',
      decription: 'é o ramo da medicina que se ocupa dos cuidados dos doentes graves ou instáveis, que emprega maior número de recursos tecnológicos e humanos no tratamento de doenças ou complicações de doenças, congregando conhecimento da maioria das especialidades médicas e outras áreas de saúde.'
   },
   {
      cod: 'nefrologista',
      label: 'Nefrologista',
      decription: 'é a parte da medicina que estuda e trata clinicamente as doenças do rim, como insuficiência renal.'
   },
   {
      cod: 'neurocirurgiao',
      label: 'Neurocirurgião',
      decription: 'atua no tratamento de doenças do sistema nervoso central e periférico passíveis de abordagem cirúrgica.'
   },
   {
      cod: 'neurologista',
      label: 'Neurologista',
      decription: 'é a parte da medicina que estuda e trata o sistema nervoso.'
   },
   {
      cod: 'nutrologo',
      label: 'Nutrologo',
      decription: 'diagnóstico, prevenção e tratamento de doenças do comportamento alimentar.'
   },
   {
      cod: 'obstetra',
      label: 'Obstetra',
      decription: 'é a area da medicina que atende, gestantes, antes do inicio da gravidez até o pós-parto.'
   },
   {
      cod: 'oftalmologista',
      label: 'Oftalmologista',
      decription: 'é a parte da medicina que estuda e trata os distúrbios dos olhos.'
   },
   {
      cod: 'ortopedista',
      label: 'Ortopedista',
      decription: 'é a parte da medicina que estuda e trata as doenças do sistema locomotor e as fraturas.'
   },
   {
      cod: 'otorrinolaringologistaa',
      label: 'Otorrinolaringologistaa',
      decription: 'é a parte da medicina que estuda e trata as doenças da orelha, nariz, seios paranasais, faringe e laringe.'
   },
   {
      cod: 'pediatra',
      label: 'Pediatra',
      decription: 'é a parte da medicina que estuda e trata crianças.'
   },
   {
      cod: 'pneumologista',
      label: 'Pneumologista',
      decription: 'é a parte da medicina que estuda e trata o sistema respiratório.'
   },
   {
      cod: 'psiquiatra',
      label: 'Psiquiatra',
      decription: 'é a parte da medicina que previne e trata ao transtornos mentais e comportamentais.'
   },
   {
      cod: 'reumatologistaa',
      label: 'Reumatologistaa',
      decription: 'é a especialidade médica que trata das doenças do tecido conjuntivo, articulações e doenças autoimunes. Diferente do senso comum o reumatologista não trata somente reumatismo.'
   },
   {
      cod: 'urologista',
      label: 'Urologista',
      decription: 'é a parte da medicina que estuda e trata cirurgicamente e clinicamente os problemas do sistema urinário e do sistema reprodutor masculino.'
   }
];

medicalInfos.getByLabel = function(label) {
	if (!medicalInfos.specializations || !label)
		return;

	var index = _.findIndex(medicalInfos.specializations, function(s) { return s.label.toLowerCase() === label.toLowerCase(); })

	if (index >= 0)
		return medicalInfos.specializations[index];
	return undefined;
}

medicalInfos.getByCod = function(cod) {
	if (!medicalInfos.specializations || !cod)
		return;

	var index = _.findIndex(medicalInfos.specializations, function(s) { return s.cod.toLowerCase() === cod.toLowerCase(); })

	if (index >= 0)
		return medicalInfos.specializations[index];
	return undefined;
}

medicalInfos.allCodes = function() {
	if (!medicalInfos.specializations)
		return;

	var codes = [];
	medicalInfos.specializations.forEach(function(s) { codes.push(s.cod); });
	return codes;
}

if (typeof module !== 'undefined') {
	module.exports = medicalInfos;
}
